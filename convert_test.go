package main

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConvert(t *testing.T) {
	tests := []struct {
		description string
		projectPath string
		reportPath  string
		wantReport  string
		in          string
	}{
		{
			description: "basic njsscan report",
			reportPath:  "testdata/njsscanReports/njsscan.json",
			wantReport:  "testdata/expect/gl-sast-report.json",
		},
		{
			description: "basic njsscan report with projectPath set",
			projectPath: "foo/bar/baz",
			reportPath:  "testdata/njsscanReports/njsscan-with-project-path.json",
			wantReport:  "testdata/expect/gl-sast-report.json",
		},
		{
			description: "basic njsscan report with incorrect matchlines",
			reportPath:  "testdata/njsscanReports/njsscan-bad-matchlines.json",
			wantReport:  "testdata/expect/gl-sast-report-bad-matchlines.json",
		},
		{
			description: "basic njsscan report with error messages",
			reportPath:  "testdata/njsscanReports/njsscan-with-errors.json",
			wantReport:  "testdata/expect/gl-sast-report-with-errors.json",
		},
	}

	for _, test := range tests {
		t.Log(test.description)
		f, err := os.Open(test.reportPath)
		defer f.Close()
		if err != nil {
			t.Fatal(err)
		}
		report, err := convert(f, test.projectPath)
		if err != nil {
			t.Fatal(err)
		}

		wantReportBytes, _ := ioutil.ReadFile(test.wantReport)

		gotReportBytes, err := json.MarshalIndent(report, "", "  ")
		if err != nil {
			t.Fatal(err)
		}

		assert.Equal(t, strings.Trim(string(wantReportBytes), "\n"), strings.Trim(string(gotReportBytes), "\n"), "should be equal")
	}
}
