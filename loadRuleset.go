package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"

	"gitlab.com/gitlab-org/security-products/analyzers/nodejs-scan/v2/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/ruleset"

	log "github.com/sirupsen/logrus"
)

func loadRuleset(projectPath string) (bool, error) {
	var removeConfig bool

	// Load custom ruleset if available
	rulesetPath := filepath.Join(projectPath, ruleset.PathSAST)
	rulesetConfig, err := ruleset.Load(rulesetPath, metadata.AnalyzerID)
	if err != nil {
		switch err.(type) {
		case *ruleset.NotEnabledError:
			log.Debug(err)
		case *ruleset.ConfigFileNotFoundError:
			log.Debug(err)
		case *ruleset.ConfigNotFoundError:
			log.Debug(err)
		case *ruleset.InvalidConfig:
			log.Fatal(err)
		default:
			return removeConfig, err
		}
	}

	if rulesetConfig != nil && len(rulesetConfig.PassThrough) != 0 {
		passThrough := rulesetConfig.PassThrough[0]
		if passThrough.Type == ruleset.PassThroughRaw {
			err := ioutil.WriteFile(filepath.Join(projectPath, njsscanConfig), []byte(passThrough.Value), 0600)
			if err != nil {
				return false, err
			}
			log.Info("Loading config from custom ruleset via raw passthrough")
		} else if passThrough.Type == ruleset.PassThroughFile {
			err := copyFile(filepath.Join(projectPath, passThrough.Value), filepath.Join(projectPath, njsscanConfig))
			if err != nil {
				return false, err
			}
			log.Infof("Loading config from custom ruleset passthrough file: %s\n", passThrough.Value)
		}
		removeConfig = true
	}

	return removeConfig, nil
}

func copyFile(from, to string) error {
	fromFile, err := os.Open(cleanPath(from))
	if err != nil {
		return err
	}
	defer WithWarning(fmt.Sprintf("Could not close file %s", from), fromFile.Close)

	toFile, err := os.OpenFile(cleanPath(to), os.O_RDWR|os.O_CREATE, 0600)
	if err != nil {
		return err
	}
	defer WithWarning(fmt.Sprintf("Could not close file %s", from), toFile.Close)

	_, err = io.Copy(toFile, fromFile)
	if err != nil {
		return err
	}
	return nil
}

// WithWarning runs the function passed as argument and prints a warning if it returns an error
func WithWarning(warning string, fun func() error) {
	err := fun()
	if err != nil {
		log.Warnf("%s (%s)\n", warning, err.Error())
	}
}
